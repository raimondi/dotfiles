echo .bash_profile

export EDITOR=vim
export VISUAL=vim
export PAGER="less -X"
export VIDIR_EDITOR_ARGS='-c :set nolist ft=vidir-ls'
export CDPATH=.:~
if test -d ~/Documents/Source; then
   CDPATH=$CDPATH:~/Documents/Source
fi

for dir in                                                        \
  /sbin                                                           \
  /bin                                                            \
  /usr/sbin                                                       \
  /usr/bin                                                        \
  /opt/local/sbin                                                 \
  /usr/local/bin                                                  \
  /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin \
  /opt/local/bin                                                  \
  ~/.local/bin                                                    \
  ~/.luarocks/bin                                                 \
  ~/Source/fzf/bin                                                \
  ~/bin                                                           \

  do
  if [[ -e "$dir" ]]; then
     export PATH=$dir:$PATH
  fi
done

export PATH=~/bin:/usr/local/bin:$PATH
# MacPorts Installer addition on 2017-02-15_at_13:46:10: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

if [ -e /Users/israel/.nix-profile/etc/profile.d/nix.sh ]; then . /Users/israel/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

if [ -z ${FIRSTSHELL+x} ] && [[ $- == *i* ]]; then
  export FIRSTSHELL=x

  if which fish > /dev/null; then
    exec $(which fish)
  fi

  if which zsh > /dev/null; then
    exec $(which zsh)
  fi
fi

export HISTFILESIZE=10000000
export HISTSIZE=10000
export HISTCONTROL=ignoredups

# crontab -e will use:
export VISUAL="/Applications/MacVim.app/Contents/MacOS/Vim"
#export VISUAL=~/bin/mvim

# Compress the cd, ls -l series of commands.
alias lc="cl"
function cl () {
   if [ $# = 0 ]; then
      cd && l -l
   else
      cd "$*" && l -l
   fi
}

# Page-up Page-down to search history.
#bind '"\M-[A":history-search-backward'
#bind '"\M-[B":history-search-forward'

# Colours:
# Use colours.sh

OC0="\[\e[0m\]"
OC1="\[\e[1m\]"
OC2="\[\e[2m\]"
OC3="\[\e[4m\]"
OC4="\[\e[5m\]"
OC5="\[\e[7m\]"
OC6="\[\e[8m\]"
FC0="\[\e[30m\]"
FC1="\[\e[31m\]"
FC2="\[\e[32m\]"
FC3="\[\e[33m\]"
FC4="\[\e[34m\]"
FC5="\[\e[35m\]"
FC6="\[\e[36m\]"
FC7="\[\e[37m\]"
FC8="\[\e[90m\]"
FC9="\[\e[91m\]"
FC10="\[\e[92m\]"
FC11="\[\e[93m\]"
FC12="\[\e[94m\]"
FC13="\[\e[95m\]"
FC14="\[\e[96m\]"
FC15="\[\e[97m\]"
BC0="\[\e[40m\]"
BC1="\[\e[41m\]"
BC2="\[\e[42m\]"
BC3="\[\e[43m\]"
BC4="\[\e[44m\]"
BC5="\[\e[45m\]"
BC6="\[\e[46m\]"
BC7="\[\e[47m\]"

if [ "$UID" = "0" ];
then
   # I am root
   CLRU="$FC1"
   CLRU2="$FC1"
else
   # Otherwise
   CLRU="$FC3"
   CLRU2="$FC3"
fi

#2:57  up 2 days, 10:16, 7 users, load averages: 0.41 0.29 0.34

function myUptime () {
   if [[ `echo $OSTYPE` == darwin* ]]
   then
      SED="sed -E"
   else
      SED="sed"
   fi
   uptime | $SED -e 's/(^.*, )([0-9]* users, )(.*)$/\1\3/' -e 's/(up [0-9]+) days?, +([0-9]+):([0-9]+)/\1d \2h \3m/' -e 's/(0\.|\.)([0-9]+)/\2%/g' -e 's/load averages/CPU/' -e 's/  / /'
}

PS1="$CLRU\u$FC6@$FC3\h$OC0:$FC5\w/$OC0 | $FC6\d, $OC0$FC2\$(myUptime)$OC0\n$CLRU2\$$OC0 "
#PS1="$CLRU\u$FC6@$FC3\h$OC0:$FC5\w/$OC0 | $FC6\d, \t$OC0\n$CLRU2\$$OC0 "
#PS1="\u@\h:\w/ | \d, \t\n\$ "
PS2="$CLRU2|$OC0 "

set show-all-if-ambiguous on
set completion-ignore-case on

alias it=ithief
alias top="top -o cpu"

function beep () {
   if [ $# == 0 ]; then
      printf "\a"
   else
      for (( COUNTER=0; COUNTER<$1; COUNTER++ )); do
         printf "\a"
      done
   fi
}
